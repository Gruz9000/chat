import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './client/App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import socketMiddlewar from './client/store';
import reducer from './client/reducers';
import { composeWithDevTools } from 'redux-devtools-extension';

const initialState = {
  messages: [],
};

const store = createStore(reducer, initialState, composeWithDevTools(applyMiddleware(socketMiddlewar(''))));

const Main = () => {
  return(
    <Provider store={store}>
      <App />
    </Provider>
  )
};

ReactDOM.render(Main(), document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
