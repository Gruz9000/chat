var express = require('express');
var router = express.Router();

/* GET messages listing. */
router.get('/history', function(req, res, next) {
  res.json();
});

module.exports = router;
