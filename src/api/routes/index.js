var express = require('express');
var router = express.Router();
var { getMessages } = require('../models');

/* GET home page. */
router.get('/', async function(req, res, next) {
  const data = await getMessages();
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  res.json(data);
});

module.exports = router;
