var mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  nickname: {
    type: String,
    unique: true,
    index: true,
  },
  avatar: {
    type: String,
    default: '',
  },
});

const Users = mongoose.model('Users', userSchema);

module.exports = Users;
