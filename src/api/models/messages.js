var mongoose = require('mongoose');

const messagesSchema = new mongoose.Schema({
  messageText: {
    type: String,
    required: true,
  },
  date: { type: Date, default: Date.now },
  nickname: { type: mongoose.Schema.Types.ObjectId, ref: 'Users' },
});

const Messages = mongoose.model('Messages', messagesSchema);

module.exports = Messages;
