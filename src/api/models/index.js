var mongoose = require('mongoose');

var Users = require('./users');
var Messages = require('./messages');

const connectorDb = () => {
  return mongoose.connect('mongodb://127.0.0.1:27017/kekbase');
};

const clearCollections = async () => {
  return await Promise.all([
    Users.deleteMany({}),
    Messages.deleteMany({}),
  ]);
};

const init = async () => {
  // Users.create({ nickname: 'init', avatar: '' }, function(err, doc) {});
};

const findByName = async (name) => {
  let user = await Users.findOne({
    nickname: name,
  });
  return user;
};

const addMessage = async (data) => {
  try {
    const user = await findByName(data.nickname);
    Messages.create({ nickname: user._id, messageText: data.message });
  } catch(err) {
    console.log(err);
  }
};

const addUser = async (data) => {
  try {
    Users.create({ nickname: data.nickname, avatar: data.avatar });
  } catch(err) {
    console.log(err);
  }
};

const getMessages = async () => {
  let response;
  try {
    response = await Messages.find({},'nickname messageText').populate('nickname');
  } catch(err) {
    console.log(err);
  }
  return response;
};

module.exports = {
  connectorDB: connectorDb,
  clearCollections: clearCollections,
  addMessage: addMessage,
  addUser: addUser,
  getMessages: getMessages,
  init: init,
};
