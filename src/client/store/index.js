import ActionTypes from '../actions';

const customMiddleWare = (url) => {
  return store => {
    let ws = new WebSocket(url || 'ws://localhost:3300');

    ws.onmessage = function (message) {
      store.dispatch({
        type : ActionTypes.GET_WEBSOCKET_MESSAGE,
        payload : message
      });
    };
    const setStatus = function (data) {
      store.dispatch({
        type : ActionTypes.OPEN_WS_STATUS,
        payload : data
      });
    };
    ws.onopen = setStatus;
    ws.onerror = setStatus;

    // const check = () => {
    //   if (!ws || ws.readyState == WebSocket.CLOSED) this.connect();
    //   //check if websocket instance is closed, if so call `connect` function.
    // };

    return next => action => {
      if(action.type === ActionTypes.SEND_WEBSOCKET_MESSAGE) {
        ws.send(JSON.stringify(action.payload));
      }
      if(action.type === ActionTypes.CREATE_WEBSOCKET_USER) {
        ws.send(JSON.stringify(action.payload));
      }

      return next(action);
    }
  }
};

export default customMiddleWare;
