import React, { Component } from 'react';
import './App.css';
import Button from '@material-ui/core/Button';
import { TextField, Avatar, Card, CardContent, Typography } from '@material-ui/core';
import { connect } from 'react-redux';
import axios from 'axios';
import { sendMessage, createUser, pullHistory } from './actions';


class ChatRoot extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nickname: '',
      saved: false,
      tempMsg: '',
    };
  }

  componentDidMount() {
    const { pullHistory } = this.props;
    axios.get('http://localhost:3013')
      .then(function (response) {
        if (response.data.length) pullHistory(response.data);
      });
  }

  setName = (event) => {
    this.setState({ nickname: event.target.value });
  };
  setText = (event) => {
    this.setState({ tempMsg: event.target.value });
  };

  saveName = () => {
    const { createUser } = this.props;
    const { nickname } = this.state;
    if (!nickname) return;
    createUser({ nickname, avatar: '' });
    this.setState({ saved: true });
  };

  sendText = () => {
    const { sendMessage } = this.props;
    const { tempMsg, nickname } = this.state;
    if (!tempMsg) return;
    sendMessage({ nickname, message: tempMsg });
    this.setState({ tempMsg: '' });
  };

  render() {
    const { nickname, saved, tempMsg } = this.state;
    const { messages } = this.props;

    return (
      <div className="App">
        {!saved && (<div className="name">
          <TextField
            id="outlined-name"
            label="Your name"
            value={nickname}
            onChange={this.setName}
            margin="normal"
            variant="outlined"
          />
          <Button variant="contained" color="primary" onClick={this.saveName}>Enter</Button>
        </div>
        )}
        <div className="chat">
          {messages.map((data, index) => {

            return (
              <div key={index} className={`item ${nickname === data.nickname ? 'mine' : 'other'}`}>
                <div className="who">
                  <Avatar src="https://image.flaticon.com/icons/svg/145/145859.svg"></Avatar>
                  <span className="owner">{data.nickname}</span>
                </div>
                <Card className="msg">
                  <CardContent>
                    <Typography className="" color="textSecondary" gutterBottom>
                      {data.message}
                    </Typography>
                  </CardContent>
                </Card>
              </div>
            )
          })
          }
        </div>
        <div className="enter">
          <TextField
            id="outlined-name"
            onChange={this.setText}
            value={tempMsg}
            margin="normal"
            variant="outlined"
          />
          <Button variant="contained" disabled={!saved} color="primary" onClick={this.sendText}>Post</Button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    messages: state.messages,
  }
};

const App = connect(
  mapStateToProps,
  { sendMessage, createUser, pullHistory },
)(ChatRoot);

export default App;
