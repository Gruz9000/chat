
import ActionTypes from '../actions';

const initialState = {
  messages: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.GET_WEBSOCKET_MESSAGE:{
      return {
        ...state,
        messages: [].concat(JSON.parse(action.payload.data), state.messages)
      };
    }
    case ActionTypes.SEND_WEBSOCKET_MESSAGE: {
      return {
        ...state,
        messages: [].concat(action.payload, state.messages)
      };
    }
    case ActionTypes.PULL_HISTORY: {
      const formated = action.payload.map(data => ({ nickname: data.nickname.nickname, message: data.messageText }));
      return {
        ...state,
        messages: [].concat(formated, state.messages)
      };
    }
    default:
      return state;
  }
};

export default reducer;
