import { createAction } from 'redux-actions';

const ActionTypes = {
  OPEN_WS_STATUS: 'OPEN_WS_STATUS',
  GET_WEBSOCKET_MESSAGE: 'GET_WEBSOCKET_MESSAGE',
  SEND_WEBSOCKET_MESSAGE: 'SEND_WEBSOCKET_MESSAGE',
  CREATE_WEBSOCKET_USER: 'CREATE_WEBSOCKET_USER',
  PULL_HISTORY: 'PULL_HISTORY',
};

export const sendMessage = createAction(ActionTypes.SEND_WEBSOCKET_MESSAGE);
export const createUser = createAction(ActionTypes.CREATE_WEBSOCKET_USER);
export const pullHistory = createAction(ActionTypes.PULL_HISTORY);

export default ActionTypes;
